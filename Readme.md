# This repository explains how to run vagrant and provision with ansible on windows.

Requirements:
  - Vagrant
  - Virtutal Box

Steps:

  - create a new folder and initialze the folder by running vagrant init
  - use the sample Vagrantfile instead of the one created with the init.
  - modify the ansible provisioning files stored inside the ansible-playbook folder
  - now use vagrant up command to setup the guest machine (this process will setup ansible on the guest machine).
  - now ssh into the guest and run the below command.
    $. /vagrant/inside-guest.sh
