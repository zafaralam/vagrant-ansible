#copy the ansible-playbook folder to root
cp -a /vagrant/ansible-playbook ~/

#remove execute from host_local
chmod -x ansible-playbook/host_local

#run playbook.yml
ansible-playbook ansible-playbook/playbook.yml -i ansible-playbook/host_local -c local
